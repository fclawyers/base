@extends('layouts.master')
@section('title')
    - Settings
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel-body">
                    <div class="panel panel-default clearfix">
                        <div class="panel-heading">
                            <h4>Account Details</h4>
                        </div>
                        <div class="panel-body">
                            <table style="border:none;">
                                <tbody>
                                <tr>
                                    <td style="padding:5px;">
                                        <span class="text-muted"><small>Name</small></span><br/>
                                        {{ $user->name }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:5px;">
                                        <span class="text-muted"><small>Email</small></span><br/>
                                        {{ $user->email }}
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding:5px;">
                                        <span class="text-muted"><small>Password</small></span>
                                        <span class="text-muted pull-right"><small><a href="{{url('/password/reset')}}">(Reset Password)</a></small></span>
                                        <br/>
                                        **********
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection