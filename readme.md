# Basic PHP Website with Authentication, Roles and Permission management

This base code is desiged to get a basic website with authentication, roles and permissions up and running with little effort. It also includes some management interfaces to allow quick creation of user accounts, roles and permissions.

##Prerequisites
1. Composer `https://getcomposer.org/`

2. Laravel `https://laravel.com/docs/5.3`

##Setup
1. Install composer `https://getcomposer.org/`

2. Install Laravel as per `https://laravel.com/docs/5.3`

3. Clone the repo: `git clone https://fclawyers@bitbucket.org/fclawyers/base.git`

4. Run `composer install` inside the cloned repo

5. Copy the .env.example file and rename it to .env

6. Update the .env settings to match your database setup

6. Run `php artisan migrate` to create the database tables

7. Run `php artisan db:seed --class=ManagementDefaultsSeeder` to add the default permissions and roles into the database and create a blank user with the following details:

    Username: `user@domain.com`

    Password: `password`
    
    Role: `root`

##Created in Laravel
Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

### Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

### Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

### Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
