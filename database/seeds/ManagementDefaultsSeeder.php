<?php

use Illuminate\Database\Seeder;

class ManagementDefaultsSeeder extends Seeder
{
    /**
     * Adds a default admin user and the default permissions and roles
     *
     * @return void
     */
    public function run()
    {
        //create default admin user
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'user@domain.com',
            'password' => bcrypt('password'),
        ]);

        $this->call(PermissionsSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(PermissionRoleSeeder::class);

        //grant root admin to user
        DB::table('role_user')->insert([
            'role_id' => 1,
            'user_id' => 1,
        ]);
    }
}
