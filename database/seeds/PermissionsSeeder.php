<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Adds the default permissions to the database
     *
     * @return void
     */
    public function run()
    {

        DB::table('permissions')->insert([
            'name' => 'manage-roles',
            'display_name' => 'Manage roles and permissions',
            'description' => 'Can view roles and permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'manage-admin',
            'display_name' => 'Manage admin',
            'description' => 'Can view admin options',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit-roles',
            'display_name' => 'Edit roles',
            'description' => 'Can edit roles',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'add-roles',
            'display_name' => 'Add roles',
            'description' => 'Can add new roles',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete-roles',
            'display_name' => 'Delete roles',
            'description' => 'Can delete roles',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'add-permissions',
            'display_name' => 'Add permissions',
            'description' => 'Can add new permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit-permissions',
            'display_name' => 'Edit permissions',
            'description' => 'Can edit permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete-permissions',
            'display_name' => 'Delete permissions',
            'description' => 'Can delete permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'manage-role-permissions',
            'display_name' => 'Manage role permissions',
            'description' => 'Can view role permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'add-role-permissions',
            'display_name' => 'Add role permissions',
            'description' => 'Can add role permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete-role-permissions',
            'display_name' => 'Delete role permissions',
            'description' => 'Can delete role permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'manage-users',
            'display_name' => 'Manage users',
            'description' => 'Can view users admin page',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'add-users',
            'display_name' => 'Add users',
            'description' => 'Can add new users',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'edit-users',
            'display_name' => 'Edit users',
            'description' => 'Can edit users',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete-users',
            'display_name' => 'Delete users',
            'description' => 'Can delete users',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'manage-user-roles',
            'display_name' => 'Manage user roles',
            'description' => 'Can view user roles',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'add-user-roles',
            'display_name' => 'Add user roles',
            'description' => 'Can add roles to users',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('permissions')->insert([
            'name' => 'delete-user-roles',
            'display_name' => 'Delete user roles',
            'description' => 'Can delete roles from users',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
