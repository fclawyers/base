<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Adds the default roles to the database
     *
     * @return void
     */
    public function run()
    {

        DB::table('roles')->insert([
            'name' => 'root',
            'display_name' => 'Root Admin',
            'description' => 'Root Admin role - can access everything regardless of permissions',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Admin role',
            'created_at' => date("Y-m-d H:i:s"),
        ]);

        DB::table('roles')->insert([
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'Basic user role',
            'created_at' => date("Y-m-d H:i:s"),
        ]);
    }
}
