<?php

namespace app\Http\Controllers\User;

use app\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class SettingsController
 * User self management controller for changing their own settings or updating their details, etc
 * @package app\Http\Controllers\User
 */
class UserAccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Shows the user settings page
     * @return mixed
     */
    public function settings()
    {
        //get user record
        $user = Auth::user();

        //display view
        return view('user/settings')->with(['user' => $user,]);
    }
}