<?php

//Autoloads all the route files contained in the Routes directory
//Do not modify the below, instead add new routes by either adding
//.php files to the routes directory or by adding to the existing
//.php files in the routes directory

foreach ( File::allFiles(__DIR__.'/routes') as $partial )
{
    require $partial->getPathname();
}
