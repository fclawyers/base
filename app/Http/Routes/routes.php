<?php

//Base URI route
Route::get('/', function() {
    return redirect('/dashboard');
});

//Dashboard route
Route::get('/dashboard', 'DashboardController@index');

/**
 * User group
 */
Route::group(['prefix' => 'user'], function() {
    Route::get('settings', 'User\UserAccountController@settings');
});
